import React, { useRef } from 'react';
import useDimensions from "react-cool-dimensions";

const RectangleResult = (props) => {

  let area = props.width * props.height;
  return (
    <div className="rectangleResult__wrapper">
      Pole powieżchni: {area}
    </div>
  )
};

export default RectangleResult;
