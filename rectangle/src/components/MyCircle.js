import React from "react";
import styled from 'styled-components';

const MyCircle = styled.div`
width: 25px;
height: 25px;
border-radius: 100%;
background: radial-gradient(circle at 10px 10px, ${props => props.backgroundColor}, #000);
top:${props => props.top + 'px'};
left: ${props => props.left + 'px'};
-webkit-box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0); 
box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0);
position: absolute;
transition: all 5s;
`;

export default MyCircle;